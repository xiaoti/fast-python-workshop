#!/bin/bash

# © 2021 ETH Zurich

set -e

for points in 10 50 100 150 200 250 300 350 400 450 500 550 600 650 700 750 800; do
  kernprof -vl benchmark_cupy.py $points | \
      grep 5000 | \
      tr -s ' ' | \
      cut -d ' ' -f 5 | \
      jq --slurp \
        '{"'$points'": {"np.einsum": .[8], "np.matmul": .[11], "np.ops": .[12], "cp.einsum": .[1], "cp.matmul": .[4], "cp.ops": .[5]}}'
  rm benchmark_cupy.py.lprof
done | \
  jq --slurp 'add' > measurements_ops.json
