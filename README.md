# About

This is the course material for the *Fast Python Workshop* offered by
[Scientific IT Services](https://sis.id.ethz.ch).

Please contact [Andrei Plamada](mailto:plamadaa@ethz.ch) or [Uwe
Schmitt](mailto:schmittu@ethz.ch) if you have questions regarding this
workshop.

## Install on your own PC

### Requirements:
- x86-64 platform (we cannot support M1 Mac for this edition of the workshop)
- Unix-like operating system: Linux, macOS, Windows Subsystem for Linux (WSL) 2 on Windows 10
- macOS only:
  1. install Xcode [Mac App Store](https://apps.apple.com/us/app/xcode/id497799835)
  2. install Command Line Tools for Xcode : open a terminal, run `xcode-select --install`

### Steps 

- clone the repo and go to `to_download` directory or download it directly using [this link](https://gitlab.ethz.ch/sis/fast-python-workshop/-/archive/main/fast-python-workshop-main.zip?path=to_download) and go to it;
- run the script `setup_conda.sh`;

This script will install miniconda on your computer (if neither Miniconda are Anaconda are installed yet), create a dedicated conda environment for the workshop and install all required packages.

The script is suited for recent Ubuntu/Debian Linux distributions (also within Windows WSL 2) and Mac, please let us know if this is not sufficient for you.

- test the installation runnning `check_setup.sh`;
- check if `start_jupyterlab.sh`  works.

In case JupyterLab does not start in your browser automatically, you might need to copy-paste the URL which you will see in the terminal into your browsers address bar.

## tldr
<details>
<summary>More about the installation</summary>

Some Python packages require system libraries and a C/C++ compiler that are not available via conda, so it is not possible to isolate everything in a conda environment.

We decided to use conda to install only the desired Python version, and to create an isolated environment. Inside the conda environment all desired packages are installed directly using pip. Moreover we installed also PyPy together with some packages for it.

In case you already use conda, keep in mind that conda is not recommended on Euler (see more [here](https://scicomp.ethz.ch/wiki/Conda)).

The main dependencies are Git, Wget, curl, Graphviz, libcurl, openBLAS, and a C/C++ compiler that accepts also the `-fopenmp` flag.  
They hardest part to get right is:
- [Pythran](https://pythran.readthedocs.io/en/latest/),
- [Cython](https://cython.org/),
- [PycURL](http://pycurl.io/),
- [NumPy](https://numpy.org/) for PyPy.

The packages can be installed using the package manager, e.g `apt` on Ubuntu, `brew` on macOS.  
For compilers we recommend [GCC](https://gcc.gnu.org/), e.g. `build-essential` package on Ubuntu, `gcc` package on `brew`.

### using conda

Once you have conda installed, you can easily activate and deactivate your `my_env` conda environment

```bash
$ conda activate my_env
$ conda deactivate
```
</details>


## Contributors
This content is restricted to contributors and it is available [here](DEVELOPMENT.md).