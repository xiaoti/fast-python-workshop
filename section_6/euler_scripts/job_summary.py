import os
import pprint
import sys
import time

time.sleep(30)

res = {
    "cwd": os.getcwd(),
    "python": sys.version,
    "hostname": os.uname().nodename,
    "pid": os.getpid(),
    "os.cpu_count() ": os.cpu_count(),
    "LSB_DJOB_NUMPROC": int(os.environ["LSB_DJOB_NUMPROC"]),
}

pprint.pprint(res)
